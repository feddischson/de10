
Synopsis
=========

The DE10 can be used with the on-board blaster or with an external debugger.

When working with Openocd, an external debugger is much faster than the on-board blaster.
For this, a FT2232H MINI Development board or a chinese clone of it can be used.

The two configuration files of can be used with such an clone when working with the DE10 board:
```
openocd -f minimodule_clone.cfg  -f ./terasic_de10_nano_soc.cfg
```
