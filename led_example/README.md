

Simple LED Example
=======================
This is just a very small design to test the Terasic DE10 SOC board. 

Expected result: binary counter whose result is connected to the LEDs.
The speed can be adjusted with the four switches (SW), SW=4'b0000 means
halt.

No software for the ARM HPS is created.

Build
========
Just run `make all` to do the whole Synthesis + Compilation.

In addition, the two targets `make program` to load the sof file and `make flash` to flash the jic file are available.


